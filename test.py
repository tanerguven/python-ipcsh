import ipcsh
from ipcsh import async, PIPE, ipcsh as sh
def log(s):
    print "[debug]%s" % s
ipcsh.debug_log = log

import os

def open_file_cout():
    try:
        import psutil
        proc = psutil.Process()
        return len(proc.open_files())
    except:
        print "WARNING: psutil library not installed. open_file_count check not working."
        return 0

def print_open_files():
    import psutil
    proc = psutil.Process()
    for f in proc.open_files():
        print f

if __name__ == '__main__':
    out = sh << "echo test" > str
    assert out == "test\n"

    out, err = sh << "echo test > /dev/stderr" > (None, str)
    assert out == None and err == "test\n"

    out = sh << "echo test" | "grep test" > str
    assert out == "test\n"

    out = sh << "echo test > /dev/stderr" &(2,1) | "grep test" > str
    assert out == "test\n"

    out = sh << "echo stdout-ok > /dev/stderr" &(2,1) > open("stdout.txt", "w")
    assert out == None
    assert open("stdout.txt").read() == "stdout-ok\n"
    assert open_file_cout() == 0

    out, err = sh << "echo stderr-ok > /dev/stderr" > (None, open("stderr.txt", "w"))
    assert out == None and err == None
    assert open("stderr.txt").read() == "stderr-ok\n"
    assert open_file_cout() == 0

    # out, err = sh("echo stderr2-ok") &(1,2) > (None, open("stderr.txt", "a"))
    # assert out == None and err == None

    out, err = sh << "echo test > /dev/stderr" &(2,1) > (str, None)
    assert out == "test\n" and err == None

    out = sh << "echo test > /dev/stderr" &(2,1) > str
    assert out == "test\n"

    out = sh << "echo test" &(1, open("/tmp/ipcsh-test-a.txt", "w")) > str
    assert out == None
    assert open("/tmp/ipcsh-test-a.txt").read() == "test\n"

    out = sh << "echo test > /dev/stderr" &(2, open("/tmp/ipcsh-test-a.txt", "w")) > str
    assert out == ""
    assert open("/tmp/ipcsh-test-a.txt").read() == "test\n"

    out, err = sh << "echo test > /dev/stderr" &(2, open("/tmp/ipcsh-test-a.txt", "w")) > (str,None)
    assert out == "" and err == None
    assert open("/tmp/ipcsh-test-a.txt").read() == "test\n"
    assert open_file_cout() == 0

    ## NOT: asagidaki gibi yazilmamali. 1 yonlendirilirse pipe ile baglamanin bir anlami yok
    # out = sh("echo test") &(1, open("/tmp/ipcsh-test-a.txt", "w")) | sh("echo test2") > str

    out = sh << "echo test > /dev/stderr" &(2, open("/tmp/ipcsh-test-a.txt", "w")) | "echo test2" > str
    assert out == "test2\n"
    assert open("/tmp/ipcsh-test-a.txt").read() == "test\n"
    assert open_file_cout() == 0
    assert sh.active_ipcsh_count == 0


    out, err = sh << "echo test > /dev/stderr" &(2, open("/tmp/ipcsh-test-a.txt", "w")) | "echo test2" > (str,None)
    assert out == "test2\n" and err == None
    assert open("/tmp/ipcsh-test-a.txt").read() == "test\n"
    assert open_file_cout() == 0
    assert sh.active_ipcsh_count == 0


    # out, err = sh << "echo test > /dev/stderr" &(2,open("/tmp/ipcsh-test-a.txt","w")) | "echo test2 > /dev/stderr && echo test3" &(2,open("/tmp/ipcsh-test-b.txt","w")) > (str,None)
    # assert out == "test3\n" and err == None
    # assert open("/tmp/ipcsh-test-a.txt").read() == "test\n"
    # assert open("/tmp/ipcsh-test-b.txt").read() == "test2\n"

    a, b = sh << "echo test > /dev/stderr" > (file, open("/tmp/ipcsh-test-a.txt", "w"))
    out, err = (sh.stdin(a) << "echo test2 > /dev/stderr && echo test3") > (str, open("/tmp/ipcsh-test-b.txt", "w"))
    assert out == "test3\n" and err == None
    assert open("/tmp/ipcsh-test-a.txt").read() == "test\n"
    assert open("/tmp/ipcsh-test-b.txt").read() == "test2\n"
    print sh.active_ipcsh_count
    assert open_file_cout() == 0
    assert sh.active_ipcsh_count == 0

    # out, err = "echo test > /dev/stderr && echo test3" &(2,open("/tmp/ipcsh-test-a.txt","w")) \
    #            | "cat" &(2,open("/tmp/ipcsh-test-b.txt","w")) > (str,None)
    # assert out == "test3\n" and err == None
    # assert open("/tmp/ipcsh-test-a.txt").read() == "test\n"
    # assert open("/tmp/ipcsh-test-b.txt").read() == ""

    out = sh << "echo test" > file
    print out.read()


    out = sh.stdin(sh << "echo test" > file) << "cat" > str
    print out

    out = sh.stdin("test\n") << "cat" > str
    assert out == "test\n"

    open("/tmp/ipcsh-test-a.txt", "w").write("test2\n")
    out = sh.stdin(open("/tmp/ipcsh-test-a.txt")) << "cat" > str
    assert out == "test2\n"
    assert sh.active_ipcsh_count == 0
    print_open_files()
    assert open_file_cout() == 0


    ################################
    r1, stdin_pipe = os.pipe()
    r1 = os.fdopen(r1)
    stdin_pipe = os.fdopen(stdin_pipe, "w")

    p = sh.stdin(r1) << "cat" > async()
    assert type(p) == async
    print p.pid

    stdin_pipe.write("test\n")
    stdin_pipe.flush()
    stdin_pipe.close()
    p.kill()
    del p
    print async.active_async_count
    assert sh.active_ipcsh_count == 0
    assert open_file_cout() == 0
    assert async.active_async_count == 0


    ################################

    r, w = os.pipe()
    r = os.fdopen(r)
    w = os.fdopen(w, "w")

    p = sh << "echo test" > async(w)
    assert type(p) == async
    print p.pid

    p.wait()

    assert w.closed == False and r.closed == False
    w.close()
    assert r.read() == "test\n"
    r.close()
    assert sh.active_ipcsh_count == 0
    assert open_file_cout() == 0

    ###############################

    p = sh << "echo test" > async(PIPE)
    assert type(p) == async
    assert type(p.stdout) == file

    assert p.stdout.read() == "test\n"
    p.wait()
    assert sh.active_ipcsh_count == 0
    assert open_file_cout() == 0

    ###############################

    p = sh << "echo test > /dev/stderr" > async(None, PIPE)
    assert type(p) == async
    assert type(p.stderr) == file

    assert p.stderr.read() == "test\n"
    p.wait()
    assert sh.active_ipcsh_count == 0
    del p
    assert async.active_async_count == 0
    assert open_file_cout() == 0

    ###############################

    p = sh << "echo test" > async(open("/tmp/ipcsh-test-a.txt", "w"))
    assert type(p) == async
    assert type(p.stdout) == file
    p.wait()
    assert async.active_async_count == 1
    del p
    assert async.active_async_count == 0
    assert sh.active_ipcsh_count == 0
    assert open_file_cout() == 0


    ###############################

    # p = sh.stdin(subprocess.PIPE) << "cat" > async(subprocess.PIPE)


    ###############################

    # import threading
    # r1, stdin_pipe = os.pipe()
    # r1 = os.fdopen(r1)
    # stdin_pipe = os.fdopen(stdin_pipe, "w")
    # stdout_pipe, w2 = os.pipe()
    # stdout_pipe = os.fdopen(stdout_pipe)
    # w2 = os.fdopen(w2, "w")
    # l = []
    # def f():
    #     p = sh.param(async=True).stdin(r1) << "cat" > w2
    #     while True:
    #         l.append(stdout_pipe.read(1))
    # thread = threading.Thread(target=f)
    # thread.start()
    # stdin_pipe.write("test\n")
    # stdin_pipe.flush()
    # import time
    # time.sleep(3)
    # print l
    # time.sleep(3)


    ###############################
    #    useShell = Flase
    ###############################

    out = sh// "echo test" > str
    print out
    assert out == "test\n"

    out = sh// "echo test" | "grep test" > str
    assert out == "test\n"

    out = sh// "echo test" &(1, open("/tmp/ipcsh-test-a.txt", "w")) > str
    assert out == None
    assert open("/tmp/ipcsh-test-a.txt").read() == "test\n"

    out = sh// "echo test" > file
    print out.read()

    out = sh.stdin(sh// "echo test" > file) // "cat" > str
    print out

    out = sh.stdin("test\n") // "cat" > str
    assert out == "test\n"

    open("/tmp/ipcsh-test-a.txt", "w").write("test2\n")
    out = sh.stdin(open("/tmp/ipcsh-test-a.txt")) // "cat" > str
    assert out == "test2\n"
    assert sh.active_ipcsh_count == 0
    print_open_files()
    assert open_file_cout() == 0


    ################################
    r1, stdin_pipe = os.pipe()
    r1 = os.fdopen(r1)
    stdin_pipe = os.fdopen(stdin_pipe, "w")

    p = sh.stdin(r1)// "cat" > async()
    assert type(p) == async
    print p.pid

    stdin_pipe.write("test\n")
    stdin_pipe.flush()
    stdin_pipe.close()
    p.kill()
    del p
    print async.active_async_count
    assert sh.active_ipcsh_count == 0
    assert open_file_cout() == 0
    assert async.active_async_count == 0


    ################################

    r, w = os.pipe()
    r = os.fdopen(r)
    w = os.fdopen(w, "w")

    p = sh // "echo test" > async(w)
    assert type(p) == async
    print p.pid

    p.wait()

    assert w.closed == False and r.closed == False
    w.close()
    assert r.read() == "test\n"
    r.close()
    assert sh.active_ipcsh_count == 0
    assert open_file_cout() == 0

    ###############################

    p = sh // "echo test" > async(PIPE)
    assert type(p) == async
    assert type(p.stdout) == file

    assert p.stdout.read() == "test\n"
    p.wait()
    assert sh.active_ipcsh_count == 0
    assert open_file_cout() == 0


    ###############################

    p = sh // "echo test" > async(open("/tmp/ipcsh-test-a.txt", "w"))
    assert type(p) == async
    assert type(p.stdout) == file
    p.wait()
    assert async.active_async_count == 1
    del p
    assert async.active_async_count == 0
    assert sh.active_ipcsh_count == 0
    assert open_file_cout() == 0

    ###############################

    assert sh.active_ipcsh_count == 0
    assert open_file_cout() == 0

    print "----------------------------"
    print "test OK"
    print "press ENTER for exit"
    raw_input()
