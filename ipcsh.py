#!/usr/bin/env python
"""
  python-ipcsh (Shell-Like IPC Library)

  Copyright (C) 2015-2017 Taner Guven <tanerguven@gmail.com>

  This library is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation; either version 3 of the
  License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of GNU Lesser General Public License
  along with this library. If not, see <http://www.gnu.org/licenses/>.
"""

import os, sys, subprocess
import inspect

__author__ = "Taner Guven"
__copyright__ = "Copyright 2015-2017 Taner Guven"
__credits__ = ["Taner Guven"]
__license__ = "LGPLv3+"
__version__ = "0.2"
__maintainer__ = "Taner Guven"
__email__ = "tanerguven@gmail.com"
__status__ = "Prototype"

debug_log = lambda x: None

PIPE = subprocess.PIPE

SUPPORTED_SYNC_OUT_TYPES = (None, PIPE, file, str)
SUPPORTED_SYNC_OUT_INSTANCES = (file,)
SUPPORTED_ASYNC_OUT_TYPES = (None, PIPE)
SUPPORTED_ASYNC_OUT_INSTANCES = (file,)


# metaclass for ipcsh static operator overloading
class ipcsh_metaclass(type):
    def __lshift__(self, other):
        debug_log("[ipcsh] %s" % other)
        if isinstance(other, ipcsh):
            return other
        elif isinstance(other, str):
            parent_scope = dict(inspect.currentframe().f_back.f_globals, **inspect.currentframe().f_back.f_locals)
            other = other % parent_scope
            return ipcsh(other)
        else:
            raise Exception("incompatible type for << operator:", type(other))

    def __floordiv__(self, other):
        a = ipcsh << other
        a.useShell = False
        return a

class async(object):

    active_async_count = 0

    def __init__(self, stdout=None, stderr=None):
        async.active_async_count += 1
        self._stdout = stdout
        self._stderr = stderr
        self._subprocess = None

        # check output type
        test = (stdout,stderr)
        for x in test:
            if x not in SUPPORTED_ASYNC_OUT_TYPES and not isinstance(x, SUPPORTED_ASYNC_OUT_INSTANCES):
                raise Exception("incompatible stdout type for async ipcsh: %s", x)

    def __del__(self):
        async.active_async_count -= 1

    @property
    def stdout(self):
        return self._stdout

    @property
    def stderr(self):
        return self._stderr

    @property
    def pid(self):
        return self._subprocess.pid

    def kill(self):
        return self._subprocess.kill()

    def wait(self):
        return self._subprocess.wait()

    @property
    def returncode(self):
        return self._subprocess.returncode

class ipcsh(object):

    __metaclass__ = ipcsh_metaclass
    active_ipcsh_count = 0 # debug

    r = 0 # last return code
    pid = -1 # last pid

    def __init__(self, cmd):
        debug_log("[__init__] '%s'" % cmd)
        ipcsh.active_ipcsh_count += 1

        self.useShell = True
        self._stdin = None
        self._stdout = None
        self._stderr = None
        self._p = None

        if cmd == None:
            self.cmd = None
            return

        # get caller functions scope
        parent_scope = dict(inspect.currentframe().f_back.f_globals, **inspect.currentframe().f_back.f_locals)
        # replace "%(xyz)" with "xyz" named variable
        cmd = cmd % parent_scope

        # FIXME: replace all escape chars
        cmd = cmd.replace('\n', '\\\\n')

        self.cmd = cmd

    def __del__(self):
        debug_log("[__del__] '%s'" % self.cmd)
        ipcsh.active_ipcsh_count -= 1

    @staticmethod
    def stdin(stdin):
        debug_log("[input] type: %s " % type(stdin))
        sh = ipcsh(None)
        if stdin==None or isinstance(stdin, file) or isinstance(stdin, str):
            sh._stdin = stdin
            return sh
        else:
            raise Exception("incompatible input type: %s" % type(stdin))

    def convert_ipcsh_if_str(self, cmd):
        if isinstance(cmd, str):
            return ipcsh(cmd)
        else:
            return cmd

    def prepare(self, stdout, stderr):
        debug_log("[prepare] '%s'" % self.cmd)

        # check & convert stderr parameter
        if self._stderr == None:
            # use default stderr
            pass
        elif self._stderr == 1:
            # redirect stderr to stdout
            stderr = subprocess.STDOUT
        elif isinstance(self._stderr, file):
            stderr = self._stderr
        else:
            raise Exception("incompatible stderr type: %s" % type(self._stderr))

        # check & convert stdout parameter
        if self._stdout == 2:
            raise Exception("&(1,2) not implemented")
        elif self._stdout in (None, subprocess.PIPE):
            # use default stdout
            pass
        elif isinstance(self._stdout, file):
            stdout = self._stdout
        elif isinstance(self._stdout, ipcsh):
            raise Exception("unused case")
        else:
            raise Exception("incompatible stdout type: %s" % type(self._stdout))

        if self.useShell == False:
            cmd = self.cmd.split(' ')
        else:
            cmd = self.cmd

        if self._stdin == None or isinstance(self._stdin, file):
            self.p = subprocess.Popen(
                cmd, shell=self.useShell, stdin=self._stdin, stdout=stdout, stderr=stderr
            )
        elif isinstance(self._stdin, ipcsh):
            self._stdin.prepare(subprocess.PIPE, stderr)
            self.p = subprocess.Popen(
                cmd, shell=self.useShell, stdin=self._stdin.p.stdout, stdout=stdout, stderr=stderr
            )
        elif isinstance(self._stdin, str):
            self.p = subprocess.Popen(
                cmd, shell=self.useShell, stdin=subprocess.PIPE, stdout=stdout, stderr=stderr
            )
            self.p.stdin.write(self._stdin)
        else:
            raise Exception("incompatible stdin type: %s" % type(self._stdin))

        return self

    def _prepare_out(self, stdout, stderr):
        # user parameters
        pipe = [None, None]
        # popen paramters
        outs = [stdout, stderr]

        for i in range(2):
            if outs[i] == None:
                outs[i] = sys.stdout
            elif isinstance(outs[i], file):
                pass # file instance ok for subprocess
            elif outs[i] == str or outs[i] == subprocess.PIPE:
                outs[i] = subprocess.PIPE
            elif outs[i] == file:
                pipe[i], outs[i]  = os.pipe()
                pipe[i] = os.fdopen(pipe[i])
                outs[i] = os.fdopen(outs[i], "w")
            else:
                raise Exception("incompatible stdout/stderr type: %s" % type(outs[i]))

        return outs[0], outs[1], pipe[0], pipe[1]

    def _run_async(self, o):

        stdout, stderr, pout, perr = self._prepare_out(o.stdout, o.stderr)

        self.prepare(stdout, stderr)

        o._subprocess = self.p
        if stdout == subprocess.PIPE:
            o._stdout = self.p.stdout
        if pout != None:
            o._stdout = pout

        if stderr == subprocess.PIPE:
            o._stderr = self.p.stderr
        if perr != None:
            o._stderr = perr

        ipcsh.r = self.p.returncode
        ipcsh.pid = self.p.pid

        return o

    def _run_default(self, o):
        # pout & perr function return values

        if isinstance(o, tuple):
            stdout, stderr = o
        else:
            stdout, stderr = (o, None)

        stdout, stderr, pout, perr = self._prepare_out(stdout, stderr)

        self.prepare(stdout, stderr)
        po, pe = self.p.communicate()
        if pout == None:
            pout = po
        if perr == None:
            perr = pe

        ipcsh.r = self.p.returncode
        ipcsh.pid = self.p.pid

        if isinstance(o, tuple):
            return pout, perr
        else:
            return pout

    # > operator : run
    def __gt__(self, o):
        debug_log("[setoutput] '%s' >" % self.cmd)

        if isinstance(o, async):
            return self._run_async(o)
        else:
            # check output type
            if isinstance(o, (list, tuple)):
                test = o
            else:
                test = (o,)
            for x in test:
                if x not in SUPPORTED_SYNC_OUT_TYPES and not isinstance(x, SUPPORTED_SYNC_OUT_INSTANCES):
                    raise Exception("incompatible stdout type for ipcsh: %s", x)
            # run default mode
            return self._run_default(o)

    # | operator : connect command or ipcsh
    def __or__(self, o):
        if type(o) == str:
            parent_scope = dict(inspect.currentframe().f_back.f_globals, **inspect.currentframe().f_back.f_locals)
            o = o % parent_scope
        o = self.convert_ipcsh_if_str(o)

        if isinstance(o, ipcsh):
            debug_log("[connect] '%s' | '%s'" % (self.cmd, o.cmd))
            o._stdin = self
            return o
        else:
            raise Exception("pipe cannot be used with non-ipcsh objects")

    # << operator : create ipcsh object
    def __lshift__(self, o):
        if type(o) == str:
            parent_scope = dict(inspect.currentframe().f_back.f_globals, **inspect.currentframe().f_back.f_locals)
            o = o % parent_scope
        assert self.cmd == None
        o = self.convert_ipcsh_if_str(o)

        if isinstance(o, ipcsh):
            debug_log("[ipcsh] '%s'" % o.cmd)
            o._stdin = self._stdin
            return o
        else:
            raise Exception("<< cannot be used with non-ipcsh objects")

    # // operator : create ipcsh object with useShell=False
    def __floordiv__(self, other):
        a = self.__lshift__(other)
        a.useShell = False
        return a

    # & operator : redirect output
    def __and__(self, o):
        if isinstance(o, tuple):
            src, dst = o
            debug_log("[redirect] '%s' %s->%s" % (self.cmd, src, dst))

            if src == 2 and (dst == 1 or isinstance(dst, file)):
                self._stderr = dst
            elif src == 1 and (dst == 2 or isinstance(dst, file)):
                self._stdout = dst
            else:
                raise Exception("incompatible redirection type: &(%s, %s)" % src, dst)

            return self

    # < operator
    def __lt__(self, o):
        raise Exception("< operator cannot be used with ipcsh objects")

    # <= operator
    def __le__(self, o):
        raise Exception("<= operator cannot be used with ipcsh objects")

    # >= operator
    def __ge__(self, o):
        raise Exception(">= operator cannot be used with ipcsh objects")
