import ipcsh
from ipcsh import echo, async, PIPE, ipcsh as sh

if __name__ == '__main__':
    ### run subprocess ###
    sh << "echo test1" > None
    # output: test1

    ### subprocess -> python communication ###
    out = sh << "echo test2" > str
    print out,
    # output: "test2\n"

    sh << "echo test3" > open("example.txt", "w")
    print open("example.txt").read(),
    # output: "test3\n"

    out = sh << "echo test4" > file
    print type(out), out.read(),
    # output: <type 'file'> "test4\n"


    ### python -> subprocess communication ###
    sh.stdin("test5\n") << "cat" > None
    # output: test5

    open("example.txt", "w").write("test6\n")
    sh.stdin(open("example.txt")) << "cat" > None
    # output: test6


    ### subprocess -> subprocess communication
    sh << "echo aaa\ntest7\nbbb\ntest7" | "grep test7" | "wc -l" > None
    # output 2


    ### async subprocess ###
    # p = sh << "sleep 3 && echo test8" > async(PIPE)
    # print "async example"
    # print "return code:", p.wait()
    # print "output:", p.stdout.read(),

    ### reading stderr ###
    out, err = sh << "echo test9a && echo test9b > /dev/stderr " > (str, str)
    print out,
    print err,
    # output: test9a\ntest9b

    ### output redirection ###
    out, err = sh << "echo test10a > /dev/stderr" &(2,1) > (str, str)
    print out, # test10a
    print err # None

    error_file = open("error_file.txt", "w")
    # out = sh << "echo test11a > /dev/stderr" &(2,error_file) | "echo test11b" &(2,error_file) > str
    out = sh << "echo test11a > /dev/stderr && echo test11b" &(2,error_file) > str
    print out, # "test11b"
    error_file.close()
    print open("error_file.txt").read(), # test11a


    ### string parameters ###
    var = "test12"
    sh << "echo %(var)s " > None
    # output: test12
